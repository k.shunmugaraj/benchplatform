#!/bin/bash

sudo add-apt-repository main
sudo add-apt-repository universe
sudo add-apt-repository restricted
sudo apt-add-repository multiverse



# Apt Postgres
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -sc)-pgdg main" > /etc/apt/sources.list.d/PostgreSQL.list'


# Apt YCSB 
sudo add-apt-repository ppa:linuxuprising/java
echo oracle-java11-installer shared/accepted-oracle-license-v1-2 select true | sudo /usr/bin/debconf-set-selections


# Apt Mongo
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list

for attempt in $(seq 1 4)
do
    sudo apt-get update -yq --fix-missing && \
    sudo apt-get install -yq oracle-java11-installer sysstat jq ec2-api-tools && \
    sudo apt-get install -yq pgbouncer && \
    sudo apt-get install -yq python3 python3-pip && \
    sudo apt-get install -yq postgresql-client-11 && \
    sudo apt-get install -yq mongodb-org-tools && \
    sudo apt-get install -yq mongodb-org-shell && \
    sudo apt-get install -yq libmongoc-dev libbson-dev luarocks && \
    sudo apt-get install -yq awscli
done

cd  /opt
sudo curl -s -O --location https://github.com/brianfrankcooper/YCSB/releases/download/0.15.0/ycsb-0.15.0.tar.gz
sudo tar xfvz ycsb-0.15.0.tar.gz
sudo wget -Oq /opt/ycsb-0.15.0/jdbc-binding/lib/postgresql-42.2.5.jar https://jdbc.postgresql.org/download/postgresql-42.2.5.jar -o /dev/null
sudo apt install -y python 

#SYSBENCH

sudo curl -s https://packagecloud.io/install/repositories/akopytov/sysbench/script.deb.sh | sudo bash

# curl -s https://packagecloud.io/install/repositories/akopytov/sysbench/script.deb.sh | sudo bash

for attempt in $(seq 1 4)
do
    sudo apt-get update -yq --fix-missing && sudo apt install -yq sysbench
    [ -f /usr/share/sysbench/tests/include/oltp_legacy/oltp.lua ] && break
done

sudo luarocks install mongorover #--local
sudo git clone https://github.com/Percona-Lab/sysbench-mongodb-lua /opt/sysbench-mongodb-lua #git@github.com:Percona-Lab/sysbench-mongodb-lua.git

