# How Snapshot works

After the initial load, the client executes a remote _stop_ on the database engine
and right after it, it executes the _makeSnapshot_ function.

This is possible thanks to the tagging system. The snapshot is called by the 
client, but it uses a volume that it has been filtered by the `Dbtagname`.


```bash
# XXX: requires testing 
function getVolumeId {
    # We do this way cause we can't tag volumes at instance creation. 
    # It should be like:
    #   aws --region $(getAttrMetadata region) ec2 describe-volumes --filter "Name=tag:Name,Values=$(getTag Dbtagname)" | jq '.Volumes[].VolumeId' -r

    devname=$(getTag Devicename)
    aws --region=$(getAttrMetadata region) ec2 describe-instances --filter "Name=tag:Name,Values=$(getTag Dbtagname)" | jq -r ".Reservations[].Instances[].BlockDeviceMappings[] | select(.DeviceName | contains(\"${devname}\")) | .Ebs.VolumeId"
}

# XXX: TODO:
function makeSnapshot {
    target=$(getTag Target)
    volid=$(getVolumeId)
    proj=$(getTag Project)
    # aws make snapstho of volumeid and name it as the TARGET
    # See data "aws_ebs_snapshot" "ebs_volume" in data_sources.tf inside this module
    # That resource uses the target name as Tag, do please TAG the snapshot
    # with Name as label and Target as key
    aws --region $(getAttrMetadata region) ec2 create-snapshot --description "${target}" --volume-id ${volid} --tag-specifications ResourceType=string.Tags=[{Key=Name,Value=${target}}]
}
```

