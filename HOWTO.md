# BenchPlatform HOWTO
### This file provides a tutorial guide to execute an automated, custom benchmark over a Postgres 11 instance.


## Initial setup
After download this repo in <repo-name>, these are the steps needed:

1. General Project Setup.
    1. Fill the <repo-name>/.env file with accessing your own AWS project.
    1. Generate the initial infrastructure and get some SSH keys:  
    ```cd <repo-name>; make setup
    make init  
    make plan  
    make apply
    ```

1. Building images:  
Images are based on Ubuntu 18.04. Skeletons for generating images for PostgreSQL (PgBouncer included), MongoDB and a generic _Client Image_ useful for accesing PostgreSQL *and* MongoDB instances are already provided. So, for compile a PostgreSQL 11 instance:  
   ```
   make TARGET=postgres image;
   make TARGET=client image
   ```  
   In this case, packer builder file will be in `packer/src/postgres.json`, and the image provisioning file will be in `packer/src/postres.sh`  

1. Configure the instance and test
This instance will be named `postgres-javabenchmark`. So, the `etc/config.yaml` file will contain:  
   ```
   postgres-javabenchmark:
     action: load # Not strictly a `load`, but still needed here.
     dbengine: "postgres" #can be _postgres_, _mongo_ or _pgbouncer_ 
     tool: "javabench" 
     fs: "xfs" ## Filesystem used for the bench
     loadsize: "fit"  ## Not used here
     dbname: "benchplatform"
     factor: 1 ## Not used here
     disk_size: 2000 # in MB
     datadir: "/opt/data"
     device_name: "/dev/xvdb"
     ebs_type: "io1"  # Storage type
     disk_iops: "5000" ## Provided IOPS
     datanode_instance_type: "m5.4xlarge"  ## Chose the appropiate hw type for your tests
     client_instance_type: "c5.xlarge"  ## Chose the appropiate hw type for your tests
     datanode_image_name: "benchplatform-postgres"  # Previously compiled image
     client_image_name: "benchplatform-client"  # Previously compiled image
     # Trigger script will contain the execution itself. In this case, will reside at  
     # `terraform/modules/compute_set/files/load/client/# postgres-javabench.sh`
     trigger_script: "postgres-javabench" 
     numrows: 1000000 ## Not used for this kind of tests
     tables: 1  ## NOt used for this kind of tests
     ## `threads`, `cycles` and `step` are variables that you can use in your `trigger_script` to generate multiple runs of the test for a given `apply`
     threads: 200  
     cycles: 5
     duration: 300
     step: 50
     ```

    The executor script, a custom-made Java tool, is in a separated, public git repository. 

1. After configuring, the build and execution phase:
     ```
     make TARGET="postgres-javabenchmark" prepare
     make TARGET="module.postgres-javabenchmark" plan
     make TARGET="module.postgres-javabenchmark" apply
    ```
     After `apply`, you should be able to locate a `postgres-javabench-clientip` section in the _Terraform Output_:
    ```
    Apply complete! Resources: 3 added, 0 changed, 0 destroyed.

    Outputs:
    ...

    postgres-javabenchmark-clientip = [
        34.200.254.1
        ]
    postgres-javabenchmark-resultsUrl = s3://benchplatform-results.s3.amazonaws.com/postgres-javabenchmark/51597b06-97a4-0abc-951d-3b1ce12e5ff2
     
    ...
    ```
    You can check the _STDOUT_ with
    `bin/check 34.200.254.1`,  or just _ssh_ the machine for a deeper view:
    `bin/jump 34.200.254.1`

    Take note of the UUID in the `postgres-javabench-resultsUrl` (will be different in your case)

1. Result Data Analysis:
    When test finishes, the results are automatically uploaded to the S3 created in the setup phase.  
    You can download (using the result uuid from above) for further analysis:
    ```bin/getResults postgres-javabenchmark/51597b06-97a4-0abc-951d-3b1ce12e5ff2```
    This will download the tests results, and also `sar` and `iotop` metrics, for the _client_ and _datanode_ instance:

    ```
    bin/getResults postgres-javabenchmark/51597b06-97a4-0abc-951d-3b1ce12e5ff2
    ```
    ```
    
    ls -lR /tmp/data/postgres-javabenchmark/51597b06-97a4-0abc-951d-3b1ce12e5ff2

    ```
    ```
    /tmp/data/postgres-javabenchmark/51597b06-97a4-0abc-951d-3b1ce12e5ff2/client-postgres-javabenchmark-1:
    total 15456
    -rw-rw-r-- 1 gerardo gerardo  1003136 jun 11 16:28 iostat.csv
    -rw-rw-r-- 1 gerardo gerardo      581 jun 11 16:28 iostat.metadata
    -rw-rw-r-- 1 gerardo gerardo      363 jun 11 16:28 metadata.json
    -rw-rw-r-- 1 gerardo gerardo     5679 jun 11 16:28 response-time-threads-16.csv
    -rw-rw-r-- 1 gerardo gerardo     5502 jun 11 16:28 response-time-threads-1.csv
    -rw-rw-r-- 1 gerardo gerardo     5655 jun 11 16:28 response-time-threads-2.csv
    -rw-rw-r-- 1 gerardo gerardo     5805 jun 11 16:28 response-time-threads-32.csv
    -rw-rw-r-- 1 gerardo gerardo     5651 jun 11 16:28 response-time-threads-4.csv
    -rw-rw-r-- 1 gerardo gerardo     5836 jun 11 16:28 response-time-threads-64.csv
    -rw-rw-r-- 1 gerardo gerardo     5667 jun 11 16:28 response-time-threads-8.csv
    -rw-rw-r-- 1 gerardo gerardo     2016 jun 11 16:28 retry-threads-16.csv
    -rw-rw-r-- 1 gerardo gerardo     1943 jun 11 16:28 retry-threads-1.csv
    -rw-rw-r-- 1 gerardo gerardo     1962 jun 11 16:29 retry-threads-2.csv
    -rw-rw-r-- 1 gerardo gerardo     2149 jun 11 16:29 retry-threads-32.csv
    -rw-rw-r-- 1 gerardo gerardo     1975 jun 11 16:29 retry-threads-4.csv
    -rw-rw-r-- 1 gerardo gerardo     2169 jun 11 16:29 retry-threads-64.csv
    -rw-rw-r-- 1 gerardo gerardo     1997 jun 11 16:29 retry-threads-8.csv
    -rw-rw-r-- 1 gerardo gerardo 13506424 jun 11 16:29 sadcfile
    -rw-rw-r-- 1 gerardo gerardo   231361 jun 11 16:29 sar_all_cpus.csv
    -rw-rw-r-- 1 gerardo gerardo    60463 jun 11 16:29 sar_contswit.csv
    -rw-rw-r-- 1 gerardo gerardo   305728 jun 11 16:29 sar_disks.csv
    -rw-rw-r-- 1 gerardo gerardo    73039 jun 11 16:29 sar_io.csv
    -rw-rw-r-- 1 gerardo gerardo    70401 jun 11 16:29 sar_load_avg.csv
    -rw-rw-r-- 1 gerardo gerardo   126684 jun 11 16:29 sar_memory.csv
    -rw-rw-r-- 1 gerardo gerardo   156104 jun 11 16:29 sar_network.csv
    -rw-rw-r-- 1 gerardo gerardo    97427 jun 11 16:29 sar_paging.csv
    -rw-rw-r-- 1 gerardo gerardo     2469 jun 11 16:30 transactions-threads-16.csv
    -rw-rw-r-- 1 gerardo gerardo     5896 jun 11 19:14 transactions-threads-16.csv.converted.csv
    -rw-rw-r-- 1 gerardo gerardo     2317 jun 11 16:30 transactions-threads-1.csv
    -rw-rw-r-- 1 gerardo gerardo     5714 jun 11 19:14 transactions-threads-1.csv.converted.csv
    -rw-rw-r-- 1 gerardo gerardo     2431 jun 11 16:30 transactions-threads-2.csv
    -rw-rw-r-- 1 gerardo gerardo     5828 jun 11 19:14 transactions-threads-2.csv.converted.csv
    -rw-rw-r-- 1 gerardo gerardo     2596 jun 11 16:30 transactions-threads-32.csv
    -rw-rw-r-- 1 gerardo gerardo     6023 jun 11 19:14 transactions-threads-32.csv.converted.csv
    -rw-rw-r-- 1 gerardo gerardo     2449 jun 11 16:30 transactions-threads-4.csv
    -rw-rw-r-- 1 gerardo gerardo     5846 jun 11 19:14 transactions-threads-4.csv.converted.csv
    -rw-rw-r-- 1 gerardo gerardo     2598 jun 11 16:30 transactions-threads-64.csv
    -rw-rw-r-- 1 gerardo gerardo     6025 jun 11 19:14 transactions-threads-64.csv.converted.csv
    -rw-rw-r-- 1 gerardo gerardo     2457 jun 11 16:30 transactions-threads-8.csv
    -rw-rw-r-- 1 gerardo gerardo     5854 jun 11 19:14 transactions-threads-8.csv.converted.csv

    /tmp/data/postgres-javabenchmark/5df4df53-a274-3514-5ba3-233a89e7c5a0/datanode-postgres-javabenchmark-1:
    total 20496
    -rw-rw-r-- 1 gerardo gerardo  1221586 jun 11 16:30 iostat.csv
    -rw-rw-r-- 1 gerardo gerardo      687 jun 11 16:30 iostat.metadata
    -rw-rw-r-- 1 gerardo gerardo    23962 jun 11 16:30 postgresql.conf
    -rw-rw-r-- 1 gerardo gerardo 18014964 jun 11 16:30 sadcfile
    -rw-rw-r-- 1 gerardo gerardo   697150 jun 11 16:30 sar_all_cpus.csv
    -rw-rw-r-- 1 gerardo gerardo    62841 jun 11 16:30 sar_contswit.csv
    -rw-rw-r-- 1 gerardo gerardo   373843 jun 11 16:30 sar_disks.csv
    -rw-rw-r-- 1 gerardo gerardo    85570 jun 11 16:31 sar_io.csv
    -rw-rw-r-- 1 gerardo gerardo    72950 jun 11 16:31 sar_load_avg.csv
    -rw-rw-r-- 1 gerardo gerardo   137218 jun 11 16:31 sar_memory.csv
    -rw-rw-r-- 1 gerardo gerardo   162781 jun 11 16:31 sar_network.csv
    -rw-rw-r-- 1 gerardo gerardo   107125 jun 11 16:31 sar_paging.csv
    ```

    You can even convert the results into a regular postgreSQL table, if that makes analysis easier for you:  
    ```bin/processJavaBenchResults postgres-javabenchmark 51597b06-97a4-0abc-951d-3b1ce12e5ff2```