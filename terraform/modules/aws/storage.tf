/*
This resource is for storing results of benchmarks
*/

resource "aws_s3_bucket" "results-bucket" {
  bucket = "${var.project}${var.s3pathsuffix}"
  tags = {
    Name        = "results-bucket"
  }
}

resource "aws_sqs_queue" "status_queue" {
  name                        = "${var.project}-status-queue.fifo" 
                              # If you happen to change the name, keep the .fifo suffix 
  fifo_queue                  = true 
  content_based_deduplication = true
  # https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/FIFO-queues.html#FIFO-queues-exactly-once-processing
}

