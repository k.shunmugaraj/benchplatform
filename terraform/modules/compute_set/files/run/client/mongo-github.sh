#!/usr/bin/env bash

# TODO
# - These are only the querys. Need review and test work
# - Add some logic and order, I guess
# - Replace corresponding variables into querys
# - Test and test

source /usr/local/bin/default.sh

tables=$(getTag Tables)
numrows=$(getTag Numrows)
dbname=$(getTag Dbname)
dbhost=$(getDbip)
threads=$(getTag Threads)
cycles=$(getTag Cycles)
duration=$(getTag Duration)
threads=$(getTag Threads)
step=$(getTag Step)
RESULTS_DIR="$(getTag Resultsdir)"

# Posible query list to elect
#querys=(a b c d)

echo START: `date +'%F_%T'`
# Please, set the query list you want to run
querys=(a b c d) # By now, all of them

# Function with client
runBench() {
	mongo --host ${dbhost} ${dbname} <<EOF
${1}
EOF
}

for i in $(seq 1 ${cycles})
do
    for q in ${querys[*]}
    do
        case ${q} in
            a)
                echo "GithubQuery-MongoDB-A: Return repositories order by most open issues quantity"
                sql_a='db.github2015.aggregate(
					[
						{ $match: {
							$and: [ { type: "IssuesEvent"} , { "payload.action" : "opened" } ] }
						},
						{ $group: { _id: "$repo.name", total: { $sum: 1 } } },
						{ $sort: { total: -1 } }
					],
					{ allowDiskUse: true, cursor: { batchSize: 100000000 } }
				)'
				{ time { runBench "${sql_a}" ; } } >> ${RESULTS_DIR}/query_A.out 2>&1
            ;;
            b)
                echo "GithubQuery-MongoDB-B: Return git event type order by quantity from major to minor"
                sql_b='db.github2015.aggregate(
					[
						{ $group: { _id: "$type", total: { $sum: 1 } } },
						{ $sort: { total: -1 } }
					],
					{ allowDiskUse: true, cursor: { batchSize: 100000000 } }
				)'	
                { time { runBench "${sql_b}" ; } } >> ${RESULTS_DIR}/query_B.out 2>&1
            ;;
            c)
                echo "GithubQuery-MongoDB-C: Return the top 10 most active actors"
                sql_c='db.github2015.aggregate(
					[
  						{ $group: { _id: "$actor.login", events: { $sum: 1 } } },
  						{ $sort: { events: -1 } },
  						{ $limit: 10 }
					],
					{ allowDiskUse: true, cursor: { batchSize: 100000000 } }
				)'
                { time { runBench "${sql_c}" ; } } >> ${RESULTS_DIR}/query_C.out 2>&1
            ;;
            d)
                echo "GithubQuery-MongoDB-D: Return repositories that have more than two comments and a specific event type, 
				order by average comments from major to minor"
                sql_d='db.github2015.aggregate(
					[
						{ $match: { "type": "PushEvent", "payload.issue.comments": { $gt : 2 } } },
						{ $group: { _id: "$repo.name", avg: { $avg: "$payload.issue.comments" } } },
						{ $sort: { avg: -1 } }
					],
					{ allowDiskUse: true, cursor: { batchSize: 100000000 } }
				)'
            	{ time { runBench "${sql_d}" ; } } >> ${RESULTS_DIR}/query_D.out 2>&1
            ;;
            *)
                echo "invalid option"
                exit 1
            ;;
        esac
    done
done

echo FINISH: `date +'%F_%T'`
exit 0