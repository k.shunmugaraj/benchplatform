#!/usr/bin/env bash
source /usr/local/bin/default.sh

tables=$(getTag Tables)
numrows=$(getTag Numrows)
dbname=$(getTag Dbname)
dbhost=$(getDbip)
threads=$(getTag Threads)
cycles=$(getTag Cycles)
duration=$(getTag Duration)
step=$(getTag Step)
target=$(getTag Target)
datestr=$(date)
action=$(getTag Action)
client=$(getTag Name)
tool=$(getTag Tool)
fs=$(getTag Fs)
dbengine=$(getTag Dbengine)
datadir=$(getTag Datadir)
RESULTS_DIR="$(getTag Resultsdir)"

makeBucket

startSadc
startIostat

until is_db_up
  do sleep 2 
done

echo "Entrypoint finished"

cat <<EOF > ${RESULTS_DIR}/metadata.json
{
  "target": "${target}",
  "fs": "${fs}",
  "tool": "${tool}",
  "dbengine": "${dbengine}",
  "tables": "${tables}",
  "numrows": "${numrows}",
  "threads": "${threads}",
  "cycles": "${cycles}",
  "duration": "${duration}",
  "step": "${step}",
  "action": "${action}",
  "start_date": "${datestr}",
  "datanode": "${dbhost}",
  "client": "${client}"
}
EOF

remote-datanode-exec "df -h" > ${RESULTS_DIR}/entrypoint_datanode_df.json
