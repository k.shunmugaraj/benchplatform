#!/usr/bin/env bash

source /usr/local/bin/default.sh

tables=$(getTag Tables)
numrows=$(getTag Numrows)
dbname=$(getTag Dbname)
dbhost=$(getDbip)
threads=$(getTag Threads)
cycles=$(getTag Cycles)
duration=$(getTag Duration)
threads=$(getTag Threads)
step=$(getTag Step)
RESULTS_DIR="$(getTag Resultsdir)"


for thread_iter in $(seq  ${threads} -${step} 1 | head -${cycles} )
do
    echo "Run: threads=${thread_iter}"
    sysbench  \
        /opt/sysbench-mongodb-lua/oltp-mongo.lua   \
        --mongodb_host=${dbhost} \
        --mongodb_db=${dbname} \
        --mongodb-port=27017 \
        --tables=${tables} \
        --threads=${thread_iter} \
        --table-size=${numrows}  \
        --time=${duration} \
        --report-interval=1 \
        --delete_inserts=0 \
        --index_updates=25 \
        --point_selects=40 \
        --simple_ranges=3 \
        --sum_ranges=1 \
        --order_ranges=3 \
        --distinct_ranges=3 \
        --non_index_updates=25  \
        --range_selects=off \
        --histogram=on \
        --range_size=0 run > ${RESULTS_DIR}/threads-${thread_iter}-5050.out


    sysbench  \
        /opt/sysbench-mongodb-lua/oltp-mongo.lua   \
        --mongodb_host=${dbhost} \
        --mongodb_db=${dbname} \
        --mongodb-port=27017 \
        --tables=${tables} \
        --threads=${thread_iter} \
        --table-size=${numrows}  \
        --time=${duration} \
        --report-interval=1 \
        --delete_inserts=0 \
        --index_updates=3 \
        --point_selects=80    \
        --simple_ranges=5 \
        --sum_ranges=1 \
        --order_ranges=5 \
        --distinct_ranges=4 \
        --non_index_updates=2  \
        --range_selects=off \
        --range_size=0 run > ${RESULTS_DIR}/threads-${thread_iter}-9505.out
done

remote-datanode-exec 'cp /var/log/mongodb/* /var/results/'
