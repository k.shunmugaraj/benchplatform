#!/usr/bin/env bash

# TODO
# - Review all SQL and change if necessary
# - Replace corresponding variables into SQL
# - Add some logic and testing to the script (/)
# - Only for Test purpose!! Please Not run yet .

source /usr/local/bin/default.sh

tables=$(getTag Tables)
numrows=$(getTag Numrows)
dbname=$(getTag Dbname)
dbhost=$(getDbip)
threads=$(getTag Threads)
cycles=$(getTag Cycles)
duration=$(getTag Duration)
threads=$(getTag Threads)
step=$(getTag Step)
RESULTS_DIR="$(getTag Resultsdir)"

# Posible query list to elect
#querys=(a b c d)

# Please, set the query list you want to run
querys=(a b c d) # By now, all of them

echo START: `date +'%F_%T'`
# Function with client
runBench() {
	/usr/bin/psql -Upostgres -d${dbname} -h${dbhost} <<EOF
\timing on
set work_mem = '1GB';
show work_mem;
${1}
EOF
}

for i in $(seq 1 ${cycles})
do
    for q in ${querys[*]}
    do
        case ${q} in
            a)
                echo "GithuQuery-PostgreSQL-A: Repositories order by most open issues quantity"
                sql_a="SELECT data->'repo'->>'name' as repo_name, count(*)
                FROM github2015 
                WHERE  (data->>'type') = 'IssuesEvent' AND (data->'payload'->>'action') = 'opened'
                GROUP BY repo_name
                ORDER BY count DESC"
                runBench "${sql_a}" >> ${RESULTS_DIR}/query_A.out
            ;;
            b)
                echo "GithuQuery-PostgreSQL-B: Return git event type (commit, push, etc) order by major quantity"
                sql_b="SELECT data->>'type' as type_event, count(*)
                FROM github2015
                GROUP BY type_event
                ORDER BY count DESC"
                runBench "${sql_b}" >> ${RESULTS_DIR}/query_B.out
            ;;
            c)
                echo "GithuQuery-PostgreSQL-C: Return the top 10 most active actors"
                sql_c="SELECT data->'actor'->>'login' as actor, count(*)
                FROM github2015
                GROUP BY actor
                ORDER BY count DESC
                LIMIT 10"
                runBench "${sql_c}" >> ${RESULTS_DIR}/query_C.out
            ;;
            d)
                echo "GithuQuery-PostgreSQL-D: Return repositories with comments and a specific event type, 
				order by average comments from major to minor"
                sql_d="SELECT 
                data->'repo'->>'name' as name, 
                avg((data->'payload'->'issue'->'comments')::int) as issue_comments     
                FROM github2015
                WHERE data->>'type' = 'PushEvent' 
                AND data->'payload'->'issue'->'comments' IS NOT NULL
                GROUP BY name
                ORDER BY issue_comments DESC"
                runBench "${sql_d}" >> ${RESULTS_DIR}/query_D.out
            ;;
            *)
                echo "invalid option"
                exit 1
            ;;
        esac
    done
done

echo FINISH: `date +'%F_%T'`
exit 0
