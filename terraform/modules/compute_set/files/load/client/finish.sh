#!/usr/bin/env bash

source /usr/local/bin/default.sh
RESULTS_DIR="$(getTag Resultsdir)"

echo "Start finish script"
# Each 5 seconds it will send messages to the queue for telling the snapshot status
while true
do
    snapstatus=$(getSnapshotStatus)
    sendMessage "Snapshot Status: ${snapstatus}" 
    if [ "x${snapstatus}" == "xcompleted" ]; then
        exit
    fi 
    sleep 5 
done &


# Stop local sadc and iostat
stopSadc
stopIostat
remote-datanode-exec "sudo pkill sadc"
remote-datanode-exec "sudo pkill iostat"

generateCSV ${RESULTS_DIR}/raw.out

# Upload local files
uploadLocalResults
remote-datanode-exec 'source /usr/local/bin/default.sh && uploadLocalResults'
echo "End finish script"


