#!/usr/bin/env bash

# TODO
# replace the range time with a variable.
# desired e.g:
# rtime="{08..12}-{01..31}-{0..23}" or rtime=${getTag Rtime}
# for file in http://data.gharchive.org/2015-${rtime}.json.gz ; do
# Could add new parameters to give range time?

source /usr/local/bin/default.sh

tables=$(getTag Tables)
numrows=$(getTag Numrows)
dbname=$(getTag Dbname)
dbhost=$(getDbip)
threads=$(getTag Threads)
tablename="github2015"
RESULTS_DIR="$(getTag Resultsdir)"

# Init
echo START: `date +'%F_%T'`

for file in http://data.gharchive.org/2015-{01..12}-{01..31}-{0..23}.json.gz
    do
        wget -q -o /dev/null -O - $file | gunzip -c | mongoimport --host=${dbhost} --db=${dbname} --collection="${tablename}" 
    done

# Create indexes 
mongo --host ${dbhost} ${dbname} <<EOF
db.github2015.createIndex( {type:1} )
db.github2015.createIndex( {"repo.name":1} )
db.github2015.createIndex( {"payload.action":1} )
db.github2015.createIndex( {"actor.login":1} )
db.github2015.createIndex( {"payload.issue.comments":1} )
EOF

makeSnapshot

echo FINISH: `date +'%F_%T'`