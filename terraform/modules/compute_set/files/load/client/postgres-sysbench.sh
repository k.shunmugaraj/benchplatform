#!/usr/bin/env bash

source /usr/local/bin/default.sh

tables=$(getTag Tables)
numrows=$(getTag Numrows)
dbname=$(getTag Dbname)
dbhost=$(getDbip)
threads=$(getTag Threads)
RESULTS_DIR="$(getTag Resultsdir)"

/usr/bin/psql -h ${dbhost} -Upostgres -c "CREATE DATABASE ${dbname}"
sysbench /usr/share/sysbench/tests/include/oltp_legacy/oltp.lua   \
    --db-driver=pgsql \
    --report-interval=1  \
    --threads=${threads} \
    --oltp-table-size=${numrows}    \
    --oltp-tables-count=${tables}  \
    --pgsql-host=${dbhost}  \
    --pgsql-port=5432 \
    --pgsql-user=postgres \
    --pgsql-db=${dbname} prepare > ${RESULTS_DIR}/raw.out 
    
# This is for reducing the restore time
/usr/bin/psql -h ${dbhost} -Upostgres -c "CHECKPOINT" ${dbname}

# Stop engine after done
remote-datanode-exec "sudo service postgresql stop"
sleep 10
# Do snapshot, use makeSnapshot() 
makeSnapshot

# Add some kind of check until snapshot is Status:completed

