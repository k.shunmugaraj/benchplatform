#cloud-config

write_files:
  - path: /usr/local/bin/entrypoint.sh
    permissions: 0755
    content: !!binary ${entrypoint}
  - path: /usr/local/bin/finish.sh
    permissions: 0755
    content: !!binary ${finish}
  - path: /usr/local/bin/${fs}.sh
    permissions: 0755
    content: !!binary ${fsscript}
  - path: /usr/local/bin/${dbengine}.sh
    permissions: 0755
    content: !!binary ${dbenginescript}
  - path: /usr/local/bin/default.sh
    permissions: 0755
    content: !!binary ${libdefault}
  - path: /usr/local/bin/trigger.sh
    permissions: 0755
    content: !!binary ${triggerscript}
    
runcmd:
  - [ sysctl,  -w, fs.file-max=2500000 ]
  - 'ulimit -n 64000'
  - [ echo, never, '>/sys/kernel/mm/transparent_hugepage/enabled']
  - [ echo, never, '>/sys/kernel/mm/transparent_hugepage/defrag']
  - [ sysctl, -w, net.ipv4.tcp_fin_timeout=1]
  - [ sysctl, -w, net.ipv4.tcp_tw_reuse=1]
  - 'mkdir ${resultsdir} && chown -R ${sshuser}: ${resultsdir}'
  - [ systemctl, daemon-reload ]
  - [ /usr/local/bin/${fs}.sh , ${device_name} , ${datadir} ]
  - [ /usr/local/bin/entrypoint.sh ]
  - [ /usr/local/bin/${dbengine}.sh, restart] 
  - 'chown -R ${sshuser}: ${resultsdir}'
  - [ /usr/local/bin/finish.sh ]

 