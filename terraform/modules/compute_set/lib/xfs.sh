#!/usr/bin/env bash

source /usr/local/bin/default.sh

function try_nvme {
    # TODO search with lsblk and do some magic
    echo "failed using $1, tyring NVME"
    mkfs.xfs /dev/nvme0n1 || echo "NVME Disk already has a format?" 
    mount -o defaults,nofail,noatime,noexec,nodiratime,noquota /dev/nvme0n1 ${2}
}

umount /mnt 2> /dev/null
mkdir $2 2> /dev/null
mkfs.xfs $1 || try_nvme $1 $2
mount -o defaults,nofail,noatime,noexec,nodiratime,noquota $1 $2
