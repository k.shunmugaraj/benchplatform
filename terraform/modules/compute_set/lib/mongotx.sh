#!/bin/bash

echo "Start mongotx.sh"
source /usr/local/bin/default.sh
RESULTS_DIR="$(getTag Resultsdir)"

cp -f /home/ubuntu/conf/mongotx/mongod.conf /etc/mongod.conf
cp /home/ubuntu/conf/mongo/mongod.conf ${RESULTS_DIR}

# Initialize data directory?

# start mongo over 27017

chown -R mongodb: /opt/data

echo "MongoDB starting UP"
service mongod ${1} 
until [[ $(is_mongo_up; echo $?) -eq 0 ]]; do 
  sleep 1; 
done

echo "rs.initiate()" | mongo
echo "MongoDB started"