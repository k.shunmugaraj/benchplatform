#!/bin/bash

source /usr/local/bin/default.sh
RESULTS_DIR="$(getTag Resultsdir)"


cp /home/ubuntu/conf/postgres/postgresql.conf /etc/postgresql/11/main/postgresql.conf
cp /home/ubuntu/conf/postgres/pg_hba.conf /etc/postgresql/11/main/pg_hba.conf


grep MemTotal /proc/meminfo | \
awk '{memkb=($2/1024)/4} END {printf("\nshared_buffers = %.0fMB\n", memkb)}' >> /etc/postgresql/11/main/postgresql.conf

grep MemTotal /proc/meminfo | \
awk '{memkb=($2/1024)-($2/1024)/4} END {printf("\effective_cache_size = %.0fMB\n", memkb)}' >> /etc/postgresql/11/main/postgresql.conf

cp /etc/postgresql/11/main/postgresql.conf ${RESULTS_DIR}


# Check if /opt/data is initialized
chown -R postgres: /opt/data
su postgres -c '/usr/lib/postgresql/11/bin/initdb -D /opt/data' 

# Start postgres over /opr/data in 5432
service postgresql ${1} 