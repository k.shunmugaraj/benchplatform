#!/usr/bin/env bash

# This is the main function library of the module. Here we have a bunch off
# functions to be used accross all scripts. Generally, a single parameter is 
# need, unless specified otherwise.

# Gets all attributes. No params
function getMetadata {
    # GCP
    #curl --silent "http://metadata.google.internal/computeMetadata/v1/instance/attributes/?recursive=true" -H "Metadata-Flavor: Google" | jq -r 'to_entries[] | flatten | join("=") '

    #AWS
    curl --silent "http://169.254.169.254/latest/meta-data/?recurse=true"
}

# Gets single attribute
function getAttrMetadata {
    #GCP
    #curl --silent "http://metadata.google.internal/computeMetadata/v1/instance/attributes/${1}" -H "Metadata-Flavor: Google"

    #AWS
    curl --silent "http://169.254.169.254/latest/meta-data/${1}"
}

# Get specified tag, this is the most widely used function.
function getTag {
    ec2-describe-tags --filter "resource-type=instance" \
        --filter "resource-id=$(getAttrMetadata instance-id)" \
        --filter "key=${1}" | cut -f5
}

# Unfortunately, getting the region in AWS isn't straight, so 
# we need to do some _jq_
# No params
function getRegion {

    # AWS
    curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq .region -r
}


# no params
function getDbip {
    benchid=$(getTag Benchid)
    dbtag=$(getTag Dbtagname)
    aws --region="$(getRegion)" ec2 describe-instances \
        --filter "Name=tag:Name,Values=${dbtag}" "Name=instance-state-name,Values=running" "Name=tag:Benchid,Values=${benchid}" |\
         jq '.Reservations[].Instances[].PublicIpAddress' -r
}

# no params
function makeBucket {
    # This function always return 0 even if the bucket already exists, so
    # we always create the bucket.
    s3uri=$(getTag S3uri)
    target=$(getTag Target)
    benchid=$(getTag Benchid)

    aws --region="$(getRegion)" s3 mb s3://${s3uri}/${target}/${benchid}
}


# no params
function getVolumeId {
    # We do this way cause we can't tag volumes at instance creation. 
    # It should be like:
    #   aws --region $(getAttrMetadata region) ec2 describe-volumes --filter "Name=tag:Name,Values=$(getTag Dbtagname)" | jq '.Volumes[].VolumeId' -r

    devname=$(getTag Devicename)
    dbtagname=$(getTag Dbtagname)
    aws --region="$(getRegion)" ec2 \
        describe-instances --filter "Name=tag:Name,Values=${dbtagname}" | jq -r ".Reservations[].Instances[].BlockDeviceMappings[] | select(.DeviceName | contains(\"${devname}\")) | .Ebs.VolumeId"
}

function getRealDevice {
    lsblk  | grep $(getTag Disksize)G | awk '{print $1}'
}

# no params
function makeSnapshot {
    target=$(getTag Target)
    volid=$(getVolumeId)
    project=$(getTag Project)
    RESULTS_DIR="$(getTag Resultsdir)"

    # proj=$(getTag Project)
    # aws make snapstho of volumeid and name it as the TARGET
    # See data "aws_ebs_snapshot" "ebs_volume" in data_sources.tf inside this module
    # That resource uses the target name as Tag, do please TAG the snapshot
    # with Name as label and Target as key
    aws --region="$(getRegion)" ec2 create-snapshot \
        --description "${target}" --volume-id ${volid} \
        --tag-specifications "ResourceType=snapshot,Tags=[{Key=Name,Value=${target}}]" > ${RESULTS_DIR}/snapshot.json

    snapshotId=$(cat ${RESULTS_DIR}/snapshot.json | jq .SnapshotId -r) 
    ec2-create-tags ${snapshotId} -t Name=${target}
    ec2-create-tags ${snapshotId} -t Project=${project}

}

# works the same as makeSnapshot but using the snapname
# useful for doing manual snapshot if you don't want to rebuild the entire
# snapshot. Run this on db
function makeManualSnapshot {
    volid=$(getVolumeId)
    project=$(getTag Project)
    RESULTS_DIR="$(getTag Resultsdir)"
    snapname=$(getTag Snapshottag)

    aws --region="$(getRegion)" ec2 create-snapshot \
        --description "${snapname}" --volume-id ${volid} \
        --tag-specifications "ResourceType=snapshot,Tags=[{Key=Name,Value=${target}}]" > ${RESULTS_DIR}/snapshot.json

    snapshotId=$(cat ${RESULTS_DIR}/snapshot.json | jq .SnapshotId -r) 
    ec2-create-tags ${snapshotId} -t Name=${snapname}
    ec2-create-tags ${snapshotId} -t Project=${project}

}


# XXX: TODO
function getSnapshotStatus {
    # --max-items is 1 to get the latest only
    target=$(getTag Target)
    # volid=$(getVolumeId)

    aws --region="$(getRegion)" ec2 describe-snapshots \
        --filter Name=tag:Name,Values=${target}  --max-items 1 | jq .Snapshots[].State -r
}


function is_db_up {
    dbengine=$(getTag Dbengine)
    is_${dbengine}_up
}

function is_mongo_up {
    dbhost=$(getDbip)
    # TODO check mongo up remotely
    mongo --host ${dbhost} 2&>1 /dev/null
}

function is_mongotx_up {
    dbhost=$(getDbip)
    # TODO check mongo up remotely
    mongo --host ${dbhost} 2&>1 /dev/null
}

function is_postgres_up {
    dbhost=$(getDbip)
    pg_isready -U postgres -h ${dbhost} -q
}

function is_pgbouncer_up {
    dbhost=$(getDbip)
    # here we as the database, not pgbouncer
    # XXX: un-harcode the port or find a better way
    pg_isready -U postgres -h ${dbhost} -p5433 -q
}

function is_postgres-edb_up {
    dbhost=$(getDbip)
    pg_isready -U postgres -h ${dbhost} -q
}


## Remote actions from client to datanode
function remote-datanode-exec {
    sshuser=$(getTag Sshuser)
    project=$(getTag Project)
    dbhost=$(getDbip)
    ssh -o "StrictHostKeyChecking  false" -i /home/${sshuser}/.ssh/id_rsa ${sshuser}@${dbhost} "${1}"
}

## Handlers

function startSadc {
    RESULTS_DIR="$(getTag Resultsdir)"

    interval=2
    /usr/lib/sysstat/sadc -S ALL ${interval} > ${RESULTS_DIR}/sadcfile &
}

function stopSadc {
    pkill sadc
    # generate CSV here for all topics
}

# this already outputs in CSV, no params
# Taken from https://github.com/ymdysk/iostat-csv/blob/master/iostat-csv.sh
function startIostat {
    RESULTS_DIR="$(getTag Resultsdir)"

    # Settings
    COMMAND_OUTPUT=$(iostat -t -x)
    CONCAT_LINE_OFFSET=3
    TITLE_LINE_OFFSET=6
    TITLE_LINE_HEADER="Date Time"

# Count command output line number
    COMMAND_OUTPUT_LINE_NUMBER=$(wc -l <<EOF
${COMMAND_OUTPUT}
EOF
)

    # Calculate how many lines to concatenate in each second
    CONCAT_LINE_NUMBER=`expr ${COMMAND_OUTPUT_LINE_NUMBER} - ${CONCAT_LINE_OFFSET}`
    # Print "N;" for ${CONCAT_LINE_NUBER} times to concatenate output lines by using sed command
    CONCAT_LINE_N=`seq -s'N;' ${CONCAT_LINE_NUMBER} | tr -d '[:digit:]'`

    # Generate title line for csv
    TITLE_AVG_CPU=$(grep avg-cpu <<EOF
${COMMAND_OUTPUT}
EOF
)

    TITLE_EACH_DEVICE=$(grep "Device" <<EOF
${COMMAND_OUTPUT}
EOF
)

    DEVICE_LINE_NUMBER=`expr ${COMMAND_OUTPUT_LINE_NUMBER} - ${TITLE_LINE_OFFSET}`
    TITLE_DEVICES=`seq -s"${TITLE_EACH_DEVICE} " ${DEVICE_LINE_NUMBER} | tr -d '[:digit:]'`
    echo "${TITLE_LINE_HEADER} ${TITLE_AVG_CPU} ${TITLE_DEVICES}" | awk 'BEGIN {OFS=","} {$1=$1;print $0}' \
    | sed 's/avg-cpu//g;s/://g;s/,,/,/g' > ${RESULTS_DIR}/iostat.metadata

    # Main part
    LANG=C; iostat -t -x 1 | grep --line-buffered -v -e avg-cpu -e Device -e Linux \
        | sed --unbuffered "${CONCAT_LINE_N}s/\n/,/g;s/\s\s*/,/g;s/,,*/,/g;s/^,//g" >> ${RESULTS_DIR}/iostat.csv &
}

function stopIostat {
    pkill iostat
}




## CSV section

# Function wrapper
# File path is the parameter
# We use all lowercase for those that come from argument
# for not having issues with the value.
function generateCSV {
    tool=$(getTag Tool)
    
    if   [ $tool == "sysbench" ]; then
        generateSysbenchCSV ${1}
    elif [ $tool == "ycsb" ]; then
        generateYcsbCSV ${1}
    fi 

}

function generateSarCSV {
    RESULTS_DIR="$(getTag Resultsdir)"

    # define file
    # sadf -dh -- -p
    sadf -dh ${RESULTS_DIR}/sadcfile -- -q > ${RESULTS_DIR}/sar_load_avg.csv
    sadf -dh ${RESULTS_DIR}/sadcfile -- -P ALL > ${RESULTS_DIR}/sar_all_cpus.csv
    sadf -dh ${RESULTS_DIR}/sadcfile -- -r > ${RESULTS_DIR}/sar_memory.csv
    sadf -dh ${RESULTS_DIR}/sadcfile -- -B > ${RESULTS_DIR}/sar_paging.csv
    sadf -dh ${RESULTS_DIR}/sadcfile -- -b > ${RESULTS_DIR}/sar_io.csv
    sadf -dh ${RESULTS_DIR}/sadcfile -- -d > ${RESULTS_DIR}/sar_disks.csv
    sadf -dh ${RESULTS_DIR}/sadcfile -- -n DEV > ${RESULTS_DIR}/sar_network.csv
    sadf -dh ${RESULTS_DIR}/sadcfile -- -w > ${RESULTS_DIR}/sar_contswit.csv

}

function generateSysbenchCSV {
    RESULTS_DIR="$(getTag Resultsdir)"

    # test
    # The following line only parses the final execution, but not the iterations
    cat ${1} | egrep " cat|threads:|transactions|deadlocks|read/write|min:|avg:|max:|percentile:" \
        | tr -d "\n" | sed 's/Number of threads: /\n/g' | sed 's/\[/\n/g' | sed 's/[A-Za-z\/]\{1,\}://g' \
        | sed 's/ \.//g' | sed -e 's/read\/write//g' -e 's/approx\.  95//g' -e 's/per sec.)//g' -e 's/ms//g' -e 's/(//g' -e 's/^.*cat //g' \
        | sed 's/ \{1,\}/,/g' >> ${1}.csv
}

function generateYcsbCSV {
    # test
    a=1
}



# you only need to pass the file path as parameter
function uploadFileS3 {
    s3uri=$(getTag S3uri)
    target=$(getTag Target)
    benchid=$(getTag Benchid)
    name=$(getTag Name)
    
    aws --region="$(getRegion)" s3 cp ${1} s3://${s3uri}/${target}/${benchid}/${name}/ --acl public-read
}

function uploadLocalResults {
    RESULTS_DIR="$(getTag Resultsdir)"
    generateSarCSV
     
    for file in $(ls ${RESULTS_DIR}/*) 
    do
        uploadFileS3 $file
    done 
}


# Function parameter is the string to send to qeue
function sendMessage {
    target=$(getTag Target)
    benchid=$(getTag Benchid)
    queueurl=$(getTag Sqsqueue)

    aws --region="$(getRegion)" \
        sqs send-message --queue-url ${queueurl} \
        --message-group-id "${target}" \
        --message-body "${target} ${benchid} - ${1}"
}
