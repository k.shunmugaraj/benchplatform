data "aws_vpc" "default" {
  default = true
}

resource "aws_vpc_ipv4_cidr_block_association" "cidr" {
  vpc_id     = "${data.aws_vpc.default.id}"
  cidr_block = "172.2.0.0/16"
}

# Use this Subnet
resource "aws_subnet" "main" {
  vpc_id     = "${aws_vpc_ipv4_cidr_block_association.cidr.vpc_id}"

  cidr_block = "172.2.0.0/16"
  availability_zone = "${var.region}a"
  tags = {
    Name = "${var.project}"
  }
}

# Deprecated!! XXX

data "aws_subnet_ids" "all" {
  vpc_id = "${data.aws_vpc.default.id}"
}



# #https://registry.terraform.io/modules/terraform-aws-modules/security-group/aws/2.15.0
module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "2.15.0"

  name        = "widesg"
  description = "Openly Security Group for BenchPlatform"
  vpc_id      = "${data.aws_vpc.default.id}"

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["all-all"]
  egress_rules        = ["all-all"]

}